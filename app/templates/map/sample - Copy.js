
(function(mapDemo, $, undefined) {
	mapDemo.Directions = (function() {
		function _Directions() {
			var    
				directionsService, directionsDisplay, 
				autoSrc, autoDest, pinA, pinB, 
				
				markerA = new google.maps.MarkerImage('m1.png',
						new google.maps.Size(24, 27),
						new google.maps.Point(0, 0),
						new google.maps.Point(12, 27)),		
				markerB = new google.maps.MarkerImage('m2.png',
						new google.maps.Size(24, 28),
						new google.maps.Point(0, 0),
						new google.maps.Point(12, 28)), 
				
				// Caching the Selectors		
				$Selectors = {
					mapCanvas: jQuery('#mapCanvas')[0], 
					dirPanel: jQuery('#directionsPanel'),
					dirInputs: jQuery('.directionInputs'),
					dirSrc: jQuery('#dirSource'),
					dirDst: jQuery('#dirDestination'),
					getDirBtn: jQuery('#getDirections'),
					dirSteps: jQuery('#directionSteps'), 
					paneToggle: jQuery('#paneToggle'), 
					useGPSBtn: jQuery('#useGPS'), 
					paneResetBtn: jQuery('#paneReset')
				},
				
				autoCompleteSetup = function() {
					//autoSrc = new google.maps.places.Autocomplete($Selectors.dirSrc[0]);
					//autoDest = new google.maps.places.Autocomplete($Selectors.dirDst[0]);
				}, // autoCompleteSetup Ends
			
				directionsSetup = function() {
					directionsService = new google.maps.DirectionsService();
					directionsDisplay = new google.maps.DirectionsRenderer({
						suppressMarkers: true
					});	
					
					directionsDisplay.setPanel($Selectors.dirSteps[0]);											
				}, // direstionsSetup Ends
				
				trafficSetup = function() {					
					// Creating a Custom Control and appending it to the map
					var controlDiv = document.createElement('div'), 
						controlUI = document.createElement('div'), 
						trafficLayer = new google.maps.TrafficLayer();
							
					jQuery(controlDiv).addClass('gmap-control-container').addClass('gmnoprint');
					jQuery(controlUI).text('Traffic').addClass('gmap-control');
					jQuery(controlDiv).append(controlUI);				
							
					// Traffic Btn Click Event	  
					google.maps.event.addDomListener(controlUI, 'click', function() {
						if (typeof trafficLayer.getMap() == 'undefined' || trafficLayer.getMap() === null) {
							jQuery(controlUI).addClass('gmap-control-active');
							trafficLayer.setMap(map);
						} else {
							trafficLayer.setMap(null);
							jQuery(controlUI).removeClass('gmap-control-active');
						}
					});							  
					map.controls[google.maps.ControlPosition.TOP_RIGHT].push(controlDiv);								
				}, // trafficSetup Ends
				
				mapSetup = function() {					
					map = new google.maps.Map($Selectors.mapCanvas, {
							zoom: 12,
							center: new google.maps.LatLng(24.8679327,67.0251453),	
							
		                    mapTypeControl: true,
		                    mapTypeControlOptions: {
		                        style: google.maps.MapTypeControlStyle.DEFAULT,
		                        position: google.maps.ControlPosition.TOP_RIGHT
		                    },
		
		                    panControl: true,
		                    panControlOptions: {
		                        position: google.maps.ControlPosition.RIGHT_TOP
		                    },
		
		                    zoomControl: true,
		                    zoomControlOptions: {
		                        style: google.maps.ZoomControlStyle.LARGE,
		                        position: google.maps.ControlPosition.RIGHT_TOP
		                    },
		                    
		                    scaleControl: true,
							streetViewControl: true, 
							overviewMapControl: true,
							 							
							mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					
					autoCompleteSetup();
					directionsSetup();
					trafficSetup();
						     var totalDistance = 0;
//-------------------------------------------------------------------------


//----------------------------------

/*var totalDuration = 0;
var legs = directionsResult.routes[0].legs;
for(var i=0; i<legs.length; ++i) {
    totalDistance += legs[i].distance.value;
    totalDuration += legs[i].duration.value;
}
$('#distance').text(totalDistance);
$('#duration').text(totalDuration);   */
var myLatLng2 = {lat: 24.8679327, lng: 67.0251453};
var myLatLng3 = {"lat": 24.935574, "lng": 67.096878};

var myLatLng4 = {lat: 24.8691325, lng: 66.9834386};
var myLatLng5 = {lat: 24.89203, lng: 67.0725643};
var myLatLng6 = {lat: 24.891586, lng: 67.0658663};
var myLatLng7 = {lat: 24.9179, lng: 67.0971};
var myLatLng8 = {lat: 24.8576, lng: 67.1196};
var myLatLng9 = {lat: 24.8518, lng: 67.0472};
var myLatLng10 = {lat: 24.93194, lng: 67.11144};
var myLatLng11= {lat: 24.9167, lng: 67.0333};
var myLatLng12 = {lat: 24.8543, lng: 67.044};
var myLatLng13= {lat: 24.8165, lng: 67.0132};
var myLatLng14 = {lat: 24.8467, lng: 67.0558};
var myLatLng15 = {lat: 24.8742, lng: 67.0737};
var myLatLng16= {lat: 24.9445, lng: 67.1386};
var myLatLng17 = {lat: 24.9431, lng: 67.0084};
var myLatLng18 = {lat: 24.8596, lng: 67.0096};
var myLatLng19 = {lat: 24.8934, lng: 67.0281};
var myLatLng20 = {lat: 24.9167, lng: 67.0833};
var myLatLng21 = {lat: 24.9484, lng: 67.1132};

//var map = new google.maps.Map(document.getElementById('map'), {
          
  //      }); 


var marker = new google.maps.Marker({
 
         position: myLatLng2,
     
     map: map,
        
  title: 'OMI Hospital'
       
 });
       
 var marker = new google.maps.Marker({
    
      position: myLatLng3,
       
   map: map,
     
     title: 'Patel Hospital'
     
   });
     
   var marker = new google.maps.Marker({
     
     position: myLatLng4,
      
    map: map,
 
         title: 'Zainab Memorial'
 
       });
      
  var marker = new google.maps.Marker({
      
    position: myLatLng5,
       
   map: map,
          
title: 'Aga Khan Hospital, Karachi'
    
    });
       
 var marker = new google.maps.Marker({
         
 position: myLatLng6,
         
 map: map,
         
 title: 'Liaqat National University Hospital'
        });
        



 /*var marker = new google.maps.Marker({
         
 position: myLatLng7,
         
 map: map,
         
 title: 'nipa_hospital'
        });
         
*/

 var marker = new google.maps.Marker({
         
 position: myLatLng8,
         
 map: map,
         
 title: 'jinnah_hospital'
        });
         

 var marker = new google.maps.Marker({
         
 position: myLatLng9,
         
 map: map,
         
 title: 'Jinnah Hospital '
        });
         


 var marker = new google.maps.Marker({
         
 position: myLatLng10,
         
 map: map,
         
 title: 'NEDUET Dispensry'
        });
         


 var marker = new google.maps.Marker({
         
 position: myLatLng11,
         
 map: map,
         
 title: 'Abbasi Shaheed Hospital '
        });
         


 var marker = new google.maps.Marker({
         
 position: myLatLng12,
         
 map: map,
         
 title: 'JPMC_Hospital'
        });
         


 var marker = new google.maps.Marker({
         
 position: myLatLng13,
         
 map: map,
         
 title: 'ZiauddinHospital'
        });
         


 var marker = new google.maps.Marker({
         
 position: myLatLng14,
         
 map: map,
         
 title: 'NationalMedicalCenter'
        });
         


 var marker = new google.maps.Marker({
         
 position: myLatLng15,
         
 map: map,
         
 title: 'HillParkGeneralHospital'
        });
         


 var marker = new google.maps.Marker({
         
 position: myLatLng16,
         
 map: map,
         
 title: ' DOWUniversityHospital'
        });
         


 var marker = new google.maps.Marker({
         
 position: myLatLng17,
         
 map: map,
         
 title: 'NationalHospital '
        });
         

 var marker = new google.maps.Marker({
         
 position: myLatLng18,
         
 map: map,
         
 title: 'civilhospital'
        });
         

 var marker = new google.maps.Marker({
         
 position: myLatLng19,
         
 map: map,
         
 title: 'Advance_Eye_Clinic'
        });
         


 var marker = new google.maps.Marker({
         
 position: myLatLng20,
         
 map: map,
         
 title: 'Akhtar_EYE_Hospital'
        });
         


 var marker = new google.maps.Marker({
         
 position: myLatLng21,
         
 map: map,
         
 title: 'KidneyAndUrinary_Hospital'
        });


				}, // mapSetup Ends 
				
				directionsRender = function(source, destination) {
					$Selectors.dirSteps.find('.msg').hide();
					directionsDisplay.setMap(map);
					
					var request = {
						origin: source,
						destination: destination,
						provideRouteAlternatives: false, 
						travelMode: google.maps.DirectionsTravelMode.DRIVING
					};		
					
					directionsService.route(request, function(response, status) {
						if (status == google.maps.DirectionsStatus.OK) {

							directionsDisplay.setDirections(response);
							
							var _route = response.routes[0].legs[0]; 
//----------------------------------------------------------------------------
/*var legs = directionsResult.routes[0].legs;
totalDistance=0;
totalDuration=0;
for(var i=0; i<legs.length; ++i) {
    totalDistance += legs[i].distance.value;
    totalDuration += legs[i].duration.value;
}
alert(totalDistance);*/
//----------------------------------------------------------------------
							pinA = new google.maps.Marker({position: _route.start_location, map: map, icon: markerA}), 
							pinB = new google.maps.Marker({position: _route.end_location, map: map, icon: markerB});																	
						}
					});
				}, // directionsRender Ends
				
				fetchAddress = function(p) {
					var Position = new google.maps.LatLng(p.coords.latitude, p.coords.longitude),  
						Locater = new google.maps.Geocoder();
					
					Locater.geocode({'latLng': Position}, function (results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							var _r = results[0];
							$Selectors.dirSrc.val(_r.formatted_address);
						}
					});
				}, // fetchAddress Ends
				
				invokeEvents = function() {
					// Get Directions
					$Selectors.getDirBtn.on('click', function(e) {
						e.preventDefault();
						var src = $Selectors.dirSrc.val(), 
							dst = $Selectors.dirDst.val();
						
						directionsRender(src, dst);
					});
					
					// Reset Btn
					$Selectors.paneResetBtn.on('click', function(e) {
						$Selectors.dirSteps.html('');
						$Selectors.dirSrc.val('');
						$Selectors.dirDst.val('');
						
						if(pinA) pinA.setMap(null);
						if(pinB) pinB.setMap(null);		
						
						directionsDisplay.setMap(null);					
					});
					
					// Toggle Btn
					$Selectors.paneToggle.toggle(function(e) {
						$Selectors.dirPanel.animate({'left': '-=305px'});
						jQuery(this).html('&gt;');
					}, function() {
						$Selectors.dirPanel.animate({'left': '+=305px'});
						jQuery(this).html('&lt;');
					});
					
					// Use My Location / Geo Location Btn
					$Selectors.useGPSBtn.on('click', function(e) {		
						if (navigator.geolocation) {
							navigator.geolocation.getCurrentPosition(function(position) {
								fetchAddress(position);
								initialize(position);
							});
							
						}
					});
				}, //invokeEvents Ends 
				
				_init = function() {
					mapSetup();
					invokeEvents();
				}; // _init Ends
				
			this.init = function() {
				_init();
				return this; // Refers to: mapDemo.Directions
			}
			return this.init(); // Refers to: mapDemo.Directions.init()
		} // _Directions Ends
		return new _Directions(); // Creating a new object of _Directions rather than a funtion
	}()); // mapDemo.Directions Ends
})(window.mapDemo = window.mapDemo || {}, jQuery);
